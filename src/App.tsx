import "./App.css";
import "carbon-components/css/carbon-components.css";
import { CNPJInput } from "./components/CNPJInput";

function App() {
  return <CNPJInput />;
}

export default App;
