import React from "react";
import { Story, Meta } from "@storybook/react";
import { Button, Form } from "carbon-components-react";
import { CNAEInput, CNAEInputProps } from "../components/CNAEInput";
import { Field, FormikErrors, FormikProps, withFormik } from "formik";
import { CNAEList } from "../data/cnae";

export default {
  title: "Inputs/CNAEInput",
  component: CNAEInput,
} as Meta;

const Template: Story<CNAEInputProps> = (args) => <CNAEInput {...args} />;
const TemplateWithValidation: Story<CNAEInputProps> = (args) => {
  return <MyForm />;
};

export const Primary = Template.bind({});
export const WithValidation = TemplateWithValidation.bind({});

interface FormValues {
  cnae: string;
}

const InnerForm = (props: FormikProps<FormValues>) => {
  const { touched, isSubmitting, errors } = props;

  return (
    <Form>
      <Field
        id="cnae"
        value={props.values.cnae || ""}
        as={CNAEInput}
        invalid={touched.cnae && !!errors.cnae}
        invalidText={errors.cnae}
        onChange={(value: string) => {
          props.handleChange({
            type: "any",
            target: { name: "cnae", value: value },
          });
        }}
      />
      <Button
        style={{ marginTop: "16px" }}
        type="submit"
        disabled={isSubmitting}
      >
        Submit
      </Button>
    </Form>
  );
};

const MyForm = withFormik<any, FormValues>({
  mapPropsToValues: () => {
    return {
      cnae: "",
    };
  },
  validate: (values: FormValues) => {
    let errors: FormikErrors<FormValues> = {};
    if (!values.cnae) errors.cnae = "CNAE é obrigatório";
    if (CNAEList.indexOf(values.cnae) === -1) errors.cnae = "CNAE inválido";
    return errors;
  },
  handleSubmit: (e, bag) => {
    console.log(e);
  },
})(InnerForm);
