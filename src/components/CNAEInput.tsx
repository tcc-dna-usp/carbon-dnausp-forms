import React from "react";
import { ComboBox, ComboBoxProps } from "carbon-components-react";
import { CNAEList } from "../data/cnae";

export type CNAEInputProps = { onChange: (s?: string) => any } & Omit<
  Partial<ComboBoxProps<string>>,
  "onChange"
>;

export const CNAEInput = (props: CNAEInputProps) => {
  const { onChange, ...parsedProps } = props;

  return (
    <ComboBox
      id="carbon-combobox"
      items={CNAEList}
      placeholder="Filtrar"
      titleText="CNAE"
      onInputChange={(evt) => {
        onChange && onChange(evt);
      }}
      onChange={() => {}}
      {...parsedProps}
    />
  );
};
