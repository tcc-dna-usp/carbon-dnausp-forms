import { TextInput } from "carbon-components-react";
import { TextInputSharedProps } from "carbon-components-react/lib/components/TextInput/props";
import { ChangeEventHandler, useState } from "react";
import MaskedInput from "react-text-mask";

export type TelefoneInputProps = {} & TextInputSharedProps;
export const TelefoneInput = (props: TelefoneInputProps) => {
  const [tel, setTel] = useState("");
  const onChange: ChangeEventHandler<HTMLInputElement> = (e) =>
    setTel(e.target.value);
  const mask = "+55 (99) 9999-9999";
  const maskWith9Digits = "+55 (99) 99999-9999";
  const maskFn = (tel: string) => {
    const is9Digits = tel.slice(-11).replace(/\D+/g, "").length > 8;

    return (is9Digits ? maskWith9Digits : mask)
      .split("")
      .map((ch) => (ch === "9" ? /[0-9]/ : ch));
  };

  return (
    <MaskedInput
      mask={maskFn}
      value={tel}
      onChange={onChange}
      {...props}
      render={(ref, props) => (
        <TextInput
          ref={ref}
          labelText="Telefone comercial"
          id="telefone-comercial"
          {...props}
        />
      )}
    />
  );
};
